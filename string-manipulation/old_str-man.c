#include <stdio.h>
#include <stdlib.h>

int main() {
	char input[4];
	scanf("%s", input);
	int n = atoi(input);

	while (n > 0) {
		char s[3000];
		char *even;
		char *odd;
		even = (char *) calloc(sizeof(s)/2, 1);
		odd = (char *) calloc(sizeof(s)/2, 1);
		int iter = 0;

		// get string from stdin
		scanf("%s", s);
		while (iter < sizeof(s)) {
			if (s[iter] == 0) break;
			int terms = iter + 1;
			if (terms % 2 == 0) {
				even[(terms / 2) - 1] = s[iter];
				iter++;
				continue;
			}
			odd[((terms + 1) / 2) - 1] = s[iter];
			iter++;
		}
		
		printf("%s %s\n", odd, even);
		free(even);
		free(odd);
		n--;
	}  
	return 0;
}
