#include <stdio.h>
#include <stdlib.h>

int main() {
	char input[4];
	scanf("%s", input);
	int n = atoi(input);

	while (n > 0) {
		char s[3000];

		// get string from stdin
		scanf("%s", s);

		//print all even numbers
		for (int i = 1; s[i] != 0; i += 2) {
			putchar(s[i]);
			s[i] = 0;
		}

		putchar(' ');

		//print all odd numbers
		for (int i = 0; s[i] != 0; i += 2) {
			putchar(s[i]);
			s[i] = 0;
		}
		
		putchar('\n');
		n--;
	}  
	return 0;
}
