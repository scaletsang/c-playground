#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include "array.h"

int push(Array *arr, Array_item item) {
  if(arr->pointer[arr->size - 1].c != 0) {
    arr->size *= 2;
    arr->pointer = (Array_item *) realloc(arr->pointer, arr->length * 2);
  }
  arr->pointer[arr->length] = item;
  arr->length++;
  return 0;
}

Array_item new_item(Array_item_type type, Array_initiator str) {
  Array_item item;
  if (type == C) {
    item.type = C;
    item.c = str.c;
    return item;
  }
  item.type = ARR;
  Array arr = new_array(0);
  for (int i = 0; str.arr[i] != 0; i++) {
    push(&arr, new_item(C, (Array_initiator) &str.arr[i]));
  }
  item.arr = &arr;
  return item;
}

Array new_array(int length, ...) { 
  Array arr;
  arr.size = 10;
  arr.length = length;
  arr.pointer = (Array_item *) malloc(arr.size * sizeof(long)); //size of an address (aka pointer)

  va_list arr_items;
  va_start(arr_items, length);

  for (int i = 0; i < length; i++) {
    Array_initiator input = va_arg(arr_items, Array_initiator);
    Array_item item;
    if (input.arr[1] == 0) {
      item = new_item(C, input);
    } else {
      item = new_item(ARR, input);
    }
    printf("%c\n", item.c);
    push(&arr, item);
  }
  
  va_end(arr_items);

  return arr;
}

Array_item pop(Array *arr) {
  if (arr->length == 0) exit(1);
  arr->length--;
  Array_item last_item = arr->pointer[arr->length];
  arr->pointer[arr->length].c = 0;
  return last_item;
}

Array concat(Array arr1, Array arr2) {
  int i = 0;
  while (arr2.pointer[i].c != 0) {
    push(&arr1, arr2.pointer[i]);
    i++;
  }
  return arr1;
}

Array slice(Array arr, int start, int end) {
  Array new_arr = new_array(0);
  for (int i = start; i <= end; i++) {
    push(&new_arr, arr.pointer[i]);
  }
  free(arr.pointer);
  return new_arr;
}

char eq(Array arr1, Array arr2) {
  if (arr1.length != arr2.length) return 0;
  int i = 0;
  while ( i < arr1.length) {
    if (arr1.pointer->type != arr2.pointer->type) return 0;
    switch (arr1.pointer->type) {
      case C:
        continue;
      case ARR:
        if (eq(*arr1.pointer->arr, *arr2.pointer->arr)) continue;
        return 0;
      }
    i++;
  }
  return 1;
}