#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>

struct dynamic_array_item;

typedef struct dynamic_array {
	size_t size;
	int length;
	struct dynamic_array_item *pointer;
} Array;

typedef enum dynamic_array_item_type {C, ARR} Array_item_type;

typedef struct dynamic_array_item {
	Array_item_type type;
	union {
		char c;
		Array *arr;
	};
} Array_item;


typedef union dynamic_array_init {
  char c;
	char *arr;
} Array_initiator;

int push(Array *arr, Array_item item);

Array_item pop(Array *arr);

Array_item new_item( Array_item_type type, Array_initiator str);

Array concat(Array arr1, Array arr2);

Array new_array(int length, ...);

Array slice(Array arr, int start, int end);

char eq(Array arr1, Array arr2);