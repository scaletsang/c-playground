#include <stdio.h>
#include <stdlib.h>

// The logic 
int _add(int a, int b) {
  int carry = a & b;
  int addition = a ^ b;
  int carry_s = carry << 1;

  while (carry_s != 0) {
    carry = carry_s & addition;
    addition = carry_s ^ addition;
    carry_s = carry << 1;
  }
  return carry_s ^ addition;
}

// Optimized version
int add(int a, int b) {
  while (a != 0){
    int temp = a;
    a = (a & b) << 1;
    b = temp ^ b;
  }
  return a ^ b;
}

int sub(int a, int b) {
  while (b != 0) {
    int temp = a;
    a = a ^ b;
    b = (b ^ (temp & b)) << 1;
  }
  return a;
}

int mult(int a, int b) {
  int result = 0;
  while (b != 0) {
    if ((b & 1) == 1) result = add(result, a);
    a <<= 1;
    b >>= 1;
  }
  return result;
}

int align(int a, int b) {
  while (b < a) b <<= 1;
  return b;
}

int divide(int a, int b) {
  int temp = b;
  int result = 0;
  b = align(a, b);
  do {
    result <<= 1;
    if (a >= b) {
      a = sub(a, b); // sub(a,b) is a self-defined a - b
      result = result | 1;
    }
    b >>= 1;
  } while (b >= temp);
  return result;
}

int main(int argc, char const *argv[]) {
  printf("%d\n", sub(375,87));
  printf("%d\n", divide(701,7));
  return 0;
}
