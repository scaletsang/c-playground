#include <stdlib.h>
#include <stdio.h>

unsigned long sum_of_multiples(unsigned long limit, int multiple) {
	unsigned long sum = 0;
	for (unsigned long i = 0; i < limit; i += multiple) {
		sum += i;
	}
	return sum;
}

int main(int argc, char const *argv[]) {
	unsigned long n = strtoul(argv[1], NULL, 10);
	unsigned long sum = sum_of_multiples(n, 3) + sum_of_multiples(n, 5) - sum_of_multiples(n, 15);
	printf("%lu", sum);
	return 0;
}
