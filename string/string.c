#include <stdlib.h>
#include <stdio.h>
#include "string.h"

int push(String *str, char item) {
	if(str->pointer[str->size - 1] != 0) {
		str->size *= 2;
		str->pointer = (char *) realloc(str->pointer, str->length * 2);
	}
	str->pointer[str->length] = item;
	str->length++;
	return 0;
}

char pop(String *str) {
	if (str->length == 0) exit(1);
	str->length--;
	char last_item = str->pointer[str->length];
	str->pointer[str->length] = 0;
	return last_item;
}

String concat(String str1, String str2) {
	int i = 0;
	while (str2.pointer[i] != 0) {
		push(&str1, str2.pointer[i]);
		i++;
	}
	return str1;
}

String newString(char string[]) {
	String str;
	str.size = 10;
	str.length = 0;
	str.pointer = (char *) malloc(str.size);

	int i = 0;
	while (string[i] != 0) {
		push(&str, string[i]);
		i++;
	}
	
	return str;
}

String slice(String str, int start, int end) {
	String new_str = newString("");
	for (int i = start; i <= end; i++) {
		push(&new_str, str.pointer[i]);
	}
	return new_str;
}

char eq(String str1, String str2) {
	if (str1.length != str2.length) return 1;
	int i = 0;
	while ( i < str1.length) {
		if (str1.pointer[i] != str2.pointer[i]) return 1;
		i++;
	}
	return 0;
}