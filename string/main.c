#include <stdio.h>
#include <stdlib.h>
#include "string.h"

String get_input() {
	char input_p[128];
	scanf("%s", input_p);
	return newString(input_p);
}

int get_int_input() {
	char input_p[4];
	scanf("%s", input_p);
	return atoi(input_p);
}

int main(int argc, char const *argv[]) {
	char stop = 0;
	while (stop == 0) {
		String input = get_input();

		// exit command
		if (eq(input, newString("exit")) == 0){
			stop = 1;
		}

		// cut string with according inputs
		if (eq(input, newString("cutstring")) == 0){
			printf("Enter a new string:\n");
			String str = get_input();
			printf("You have entered: %s\nLet's cut this string!\nPlease specify the starting and ending index to to cut the string.\nEnter a starting index:\n", str.pointer);
			int start_i = get_int_input();
			printf("Enter an ending index:\n");
			int end_i = get_int_input();
			String sliced_str = slice(str, start_i, end_i);
			printf("Original string: %s\nSliced string:   %s\n", str.pointer, sliced_str.pointer);
			free(str.pointer);
		}
		
		free(input.pointer);
	}
	return 0;
}