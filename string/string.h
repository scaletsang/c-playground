#include <stdlib.h>
#include <stdio.h>

typedef struct dynamic_array {
	size_t size;
	int length;
	char *pointer;
} String;

int push(String *str, char item);

char pop(String *str);

String concat(String str1, String str2);

String newString(char string[]);

String slice(String str, int start, int end);

char eq(String str1, String str2);