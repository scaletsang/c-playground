///////////////////////////////
//// Sieve of Eratosthenes ////
///////////////////////////////

const fs = require('fs')

function main() {
	console.time("process time")

	let n = process.argv[2]
	let numbers = new Array(n).fill(0)
	let primes = []
	
	let iter = 2
	while (iter < n) {
		if(numbers[iter] === 1) {iter++; continue}
		for (let j = iter; j < n; j += iter) {
			numbers[j] = 1
		}
		primes.push(iter++)
	}

	// Write the list of primes to a file
	fs.writeFile('./primes-generated-with-js-fast.txt', primes.join(','), err => {})
	console.timeEnd("process time")
}

main()