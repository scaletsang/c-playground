///////////////////////////////
// Summation of primes using //
//// Sieve of Eratosthenes ////
///////////////////////////////

#include <stdlib.h>
#include <stdio.h>

int main(int argc, char *args[]) {
	// upper bound of the sieve
	unsigned long n = strtoul(args[1], NULL, 10);
	unsigned long long sum = 0;
	char *numbers;
	numbers = (char *) malloc(n);

	unsigned long iter = 2;
	while(iter < n) {
		if(numbers[iter] == 1) {iter++; continue;}
		for(unsigned long j = iter; j < n; j += iter) {
			numbers[j] = 1;
		}
		sum += iter++;
	}
	printf("%llu", sum);
	free(numbers);
	return 0;
}