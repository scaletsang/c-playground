const fs = require('fs')
console.time("process time")

let primes = [2]

iteration:
for(let i = 3; i < process.argv[2]; i++) {
	for(let p = 0; p < primes.length; p++) {
		// if the current value is divisible by a prime in the list, 
		// continue to iterate on the next value
		if ((i % primes[p]) === 0) {continue iteration} 
	}
	primes.push(i)
}

fs.writeFile('./primes-generated-with-js.txt', primes.join(','), err => {})
console.timeEnd("process time")