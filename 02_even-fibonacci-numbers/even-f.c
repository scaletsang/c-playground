#include <stdlib.h>
#include <stdio.h>

unsigned long fib(int n) {
	if (n == 0) return 0;
	if (n == 1 || n == 2) return 1;
	return fib(n - 2) + fib(n - 1);            
}

int main(int argc, char const *argv[]) {
	unsigned long limit = strtoul(argv[1], NULL, 10);

	unsigned long sum = 0;
	unsigned long fib1 = 1;
	unsigned long fib2 = 2;
	unsigned long fib = 0;

	while ( fib < limit ) {
		fib = fib1 + fib2;
		if ((fib % 2) == 0) sum += fib;
	}
	
	printf("%lu", sum);
	return 0;
}
